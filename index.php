<?php
// Établir la connexion à la base de données en utilisant les constantes définies
$connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PORT);

// Vérifier la connexion
if ($connection->connect_error) {
    die("La connexion a échoué : " . $connection->connect_error);
}

// Établir la connexion à la base de données en utilisant les constantes définies
$connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PORT);

// Vérifier la connexion
if ($connection->connect_error) {
    die("La connexion a échoué : " . $connection->connect_error);
}

// Récupérer les tâches triées par date de création (du plus récent au plus ancien)
$query = "SELECT * FROM todo ORDER BY created_at DESC";
$result = $connection->query($query);

// Vérifier s'il y a des résultats
if ($result->num_rows > 0) {
    // Créer un tableau pour stocker les tâches
    $taches = array();

    // Récupérer chaque ligne de résultat et l'ajouter au tableau $taches
    while ($row = $result->fetch_assoc()) {
        $taches[] = $row;
    }
} else {
    echo "Aucune tâche trouvée.";
}

// Fermer la connexion à la base de données
$connection->close();

// Vérifier s'il y a une action reçue en POST
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['action'])) {
        $action = $_POST['action'];
        
        // Traitement en fonction de l'action reçue
        switch ($action) {
            case 'new':
                // Logique pour ajouter une nouvelle tâche à la table "todo"
                // Utilisez les données postées pour insérer une nouvelle tâche dans la table
                break;
            case 'delete':
                // Logique pour supprimer une tâche de la table "todo"
                // Utilisez l'ID posté pour supprimer la tâche correspondante de la table
                break;
            case 'toggle':
                // Logique pour basculer l'état "done" d'une tâche dans la table "todo"
                // Utilisez l'ID posté pour mettre à jour le champ "done" de la tâche correspondante
                break;
            default:
                // Action non reconnue
                break;
        }
    }
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="#">Ma Liste de Tâches</a>
    <!-- Vous pouvez ajouter des liens ou d'autres éléments ici -->
  </div>
</nav>

<!-- Formulaire d'ajout de tâches -->
<form action="" method="post">
  <div class="input-group mt-3">
    <input type="text" class="form-control" placeholder="Ajouter une nouvelle tâche" name="title">
    <button class="btn btn-primary" type="submit" name="action" value="new">Ajouter</button>
  </div>
</form>
<!-- Liste des tâches -->
<ul class="list-group mt-3">
  <?php foreach ($taches as $tache) : ?>
    <li class="list-group-item <?php echo $tache['done'] ? 'list-group-item-success' : 'list-group-item-warning'; ?>">
      <?php echo $tache['title']; ?>
      <!-- Formulaire pour chaque tâche -->
      <form action="" method="post" class="float-end">
        <input type="hidden" name="id" value="<?php echo $tache['id']; ?>">
        <button type="submit" name="action" value="toggle" class="btn btn-sm btn-info">Toggle</button>
        <button type="submit" name="action" value="delete" class="btn btn-sm btn-danger">Supprimer</button>
      </form>
    </li>
  <?php endforeach; ?>
</ul>

</body>
</html>
